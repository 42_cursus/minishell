/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_convert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 11:04:15 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/14 15:13:18 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_list  *env_convert_to_list(char **env)
{
    t_list  *l_env;

	l_env = NULL;
    if (!env)
        return (NULL);
    while (*env)
    {
        ft_lstpushback(&l_env, ft_lstnew(init_one_env(*env), ft_strlen(*env)));
        env++;
    }
    return (l_env);
}

char    **env_convert_to_array(t_list *env)
{
	char    **a_env;
	size_t  length;

    if (!env)
		return (NULL);
	length = ft_lstsize(env);
	if (!(a_env = (char**)ft_memalloc(sizeof(char*) * length + 1)))
		ft_memalloc_error("env_convert_to_array -> a_env");
	while(env)
	{
		*a_env = get_one_env_full((t_env*)env->content)
		a_env++;
		env = env->next;
	}
	*a_env = 0x0;
	return(a_env);
}

char    *env_one_convert_to_array(t_env *env)
{
    char    *tmp;

    tmp = ft_strjoin(env->name, "=");
    return (ft_strmerge(tmp, ft_strdup(env->value)));
}

t_env	*env_one_convert_to_list(char *env)
{
	t_env	*e;

	if (!(e = (t_env*)ft_memalloc(sizeof(t_env))))
		ft_memalloc_error("init_env -> e");
	e->name = ft_strrchr(env, '=');
	e->value = ft_strchr(env, '=');
    ft_putendl(e->name);
	return (e);
}