/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 13:25:32 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/14 15:10:42 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <pwd.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct	s_av
{
    int			argc;
	char		*cmd;
	char		**args;
	char		**all;
}				t_av;

typedef struct  s_env
{
    char    *name;
    char    *value;
}               t_env;

typedef struct  s_shell 
{
    t_list  *env;
    char    *cmd;
}               t_shell;

t_shell     *init_shell(char **env);

t_list      *env_convert_to_list(char **env);
char        **env_convert_to_array(t_list *env);

t_env	    *init_one_env(char *env);
t_env	    *get_one_env(t_shell *sh, char *name);
t_bool      set_one_env(t_shell *sh, char *name, char *new_value);
char        *get_one_env_full(t_env *e);

#endif