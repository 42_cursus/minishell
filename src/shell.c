/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 10:49:56 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/14 14:14:47 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_shell     *init_shell(char **env)
{
    t_shell *sh;

    if (!(sh = (t_shell*)ft_memalloc(sizeof(t_shell))))
        ft_memalloc_error("init_shell -> sh");
    sh->cmd = NULL;
    sh->env = env_convert_to_list(env);
    return (sh);
}