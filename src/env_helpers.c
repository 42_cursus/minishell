/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_helpers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 14:27:43 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/14 15:11:58 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*get_one_env(t_shell *sh, char *name)
{
	while (sh->env)
	{
		if (ft_strcmp(((t_env*)sh->env->content)->name, name) == 0)
			return ((t_env*)sh->env->content);
		sh->env = sh->env->next;
	}
	return (NULL);
}

t_bool  set_one_env(t_shell *sh, char *name, char *new_value)
{
    while (sh->env)
	{
		if (ft_strcmp(((t_env*)sh->env->content)->name, name) == 0)
		{
            ft_strdel(&((t_env*)sh->env->content)->value);
            ((t_env*)sh->env->content)->value = ft_strdup(new_value);
            return (TRUE);
        }
		sh->env = sh->env->next;
	}
    return (FALSE);
}

char    *get_one_env_full(t_env *e)
{
    char    *tmp;

    tmp = ft_strjoin(e->name, "=");
    return (ft_strmerge(tmp, ft_strdup(e->value)));
}